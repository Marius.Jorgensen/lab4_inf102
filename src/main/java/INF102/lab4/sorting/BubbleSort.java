package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (int i = 1; i < n; i++) {
                if (list.get(i - 1).compareTo(list.get(i)) > 0) {
                    swap(list, i - 1, i);
                    swapped = true;
                }
            }
            n--;
        }
    }
    

    private <T> void swap(List<T> list, int i, int j) {
        T swap = list.get(i);
        list.set(i, list.get(j));
        list.set(j, swap);
    }
}
