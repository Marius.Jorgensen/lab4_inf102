package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {
    private Random random = new Random();


    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list

        int medianIndex = listCopy.size() / 2;
        sort(listCopy, medianIndex);
        return listCopy.get(medianIndex);
        
    }

    private <T extends Comparable<T>> void sort(List<T> list, int medianIndex){
        T pivot = list.get(random.nextInt(list.size()));
        List<T> first = new ArrayList<>();
        List<T> pivo = new ArrayList<>();
        List <T> last = new ArrayList<>();

        for (T element : list){
            int compare = element.compareTo(pivot);
            if (compare < 0){
                first.add(element);
            } else if (compare > 0){
                last.add(element);
            } else {
                pivo.add(element);
            }
        }
    

    
    list.clear();
    list.addAll(first);
    list.addAll(pivo);
    list.addAll(last);

    if (list.get(medianIndex).compareTo(pivot) == 0){
        return;
    }

    if(first.size() > medianIndex){
        sort(first, medianIndex);
        list.clear();
        list.addAll(first);
    }
    else {
        sort(last, medianIndex - first.size() - pivo.size());
        list.clear();
        list.addAll(first);
        list.addAll(pivo);
        list.addAll(last);
    }

}
}
